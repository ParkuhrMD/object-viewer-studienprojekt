<?php 

require_once "classes/util.php";

if(isset($_GET['modelAcceskey']) && !empty($_GET['modelAcceskey'])){

	$return = array();

	if(isset($_GET['komponentId']) && !empty($_GET['komponentId'])){
		$sql = "SELECT * FROM `".$mysql_database."`.`ModleDaten` INNER JOIN `".$mysql_database."`.`Models` ON `Models`.`modelId`=`ModleDaten`.`ModelId` WHERE `Models`.`modelAcceskey` = '".$_GET['modelAcceskey']."' AND `ModleDaten`.`Key` ='".$_GET['komponentId']."';";
		$return["debug"][] = $sql;
		$result = $mysql->query($sql);
	    if ($result && $result->num_rows > 0) {
	    	$row = $result->fetch_assoc();
	        $return["Data"] = json_decode($row["Value"]);
	    } else {
	    	$return["debug"][] = "No DB result!";
	    }
	}

	echo json_encode($return);

} else if($_SESSION["angemeldet"]){ 

	$return = array();

	if(isset($_GET['modelId']) && !empty($_GET['modelId'])){
		if(isset($_GET['komponentId']) && !empty($_GET['komponentId'])){

			$sql = "SELECT * FROM `".$mysql_database."`.`ModleDaten` WHERE `ModelId` = '".$_GET['modelId']."' AND `Key` = '".$_GET['komponentId']."';";
			$result = $mysql->query($sql);
            if ($result && $result->num_rows > 0) {
            	$row = $result->fetch_assoc();
                $return["Data"] = json_decode($row["Value"]);
            } else {
            	$return["debug"][] = "No DB result!";
            }
		}
	}

	echo json_encode($return);


} else {
    http_response_code(401); //Unauthorized
    die("Error: Ungültiger Nutzer.");
}

?>