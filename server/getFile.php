<?php

require_once "classes/util.php";

if(isset($_GET['modelAcceskey']) && !empty($_GET['modelAcceskey'])){
    $sql = "SELECT * FROM `".$mysql_database."`.`Models` WHERE `modelAcceskey` = '".$_GET['modelAcceskey']."';";
    $result = $mysql->query($sql);

    //Nur ein Model mit der modelAcceskey solte existiert
    if ($result->num_rows == 1) {
        $data = $result->fetch_assoc();
        // Unterscheidung ob eine File zurückgegeben werden soll oder das ganze Verzeichnis
        if(isset($_GET['filename']) && !empty($_GET['filename'])){
            $filepath = $modelsDir."/".$data["modelOwner"]."/".$data['modelId']."/".$_GET['filename'];
            printFile($filepath);
        }else{
            $zip_file = $modelsDir."/".$data['modelName'].'.zip';
            $rootpath = $modelsDir."/".$data["modelOwner"]."/".$data['modelId']."/";
            zipFolder($zip_file,$rootpath);
            printFile($zip_file);
        }
    } else {
        http_response_code(404); //Not Found
        die("Error: File not found."); 
    }
} else if($_SESSION["angemeldet"]){ 
    if(isset($_GET['modelId']) && !empty($_GET['modelId'])){
        if(isset($_GET['filename']) && !empty($_GET['filename'])){
            $attachment_location = $modelsDir."/".$_SESSION["userData"]["id"]."/".$_GET['modelId']."/".$_GET['filename'];
            printFile($attachment_location);
        } else {
            $zip_file = $modelsDir."/".$_GET['modelId'].'.zip';
            $rootpath = $modelsDir."/".$_SESSION["userData"]["id"]."/".$_GET['modelId'];
            zipFolder($zip_file,$rootpath);
            printFile($zip_file);
        }
    } else {
        http_response_code(400); //Bad Request
        die("Error: Wrong Parameter.");
    }
} else {
    http_response_code(401); //Unauthorized
    die("Error: Ungültiger Nutzer.");
}

function printFile($filePath)
{
    if (file_exists($filePath)) {
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: private"); 
        header("Content-Type: text/plain");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length:".filesize($filePath));
        header("Content-Disposition: attachment; filename=".basename($filePath));
        readfile($filePath);
        die();        
    } else {
        http_response_code(404); //Not Found
        die("Error: File not found.");
    }
}

function zipFolder($zip_file,$rootpath){
    // Initialize archive object
    $zip = new ZipArchive();
    $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootpath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            // Shows server File-Stucktur  
            //$relativePath = substr($filePath, strlen($rootpath) + 1);
            // HotFix
            $relativePath = "./".basename($file);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
}
?>