-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 01. Mrz 2017 um 17:17
-- Server Version: 5.6.35-log
-- PHP-Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `project_objectviewer`
--
CREATE DATABASE IF NOT EXISTS `project_objectviewer` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `project_objectviewer`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Benutzer`
--

CREATE TABLE IF NOT EXISTS `Benutzer` (
`id` int(32) NOT NULL,
  `nutzername` text NOT NULL,
  `password` text NOT NULL,
  `acceskey` int(11) DEFAULT NULL,
  `lastlogin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Models`
--

CREATE TABLE IF NOT EXISTS `Models` (
`modelId` int(11) NOT NULL,
  `modelName` text NOT NULL,
  `modelOwner` int(11) NOT NULL,
  `modelAcceskey` text
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ModleDaten`
--

CREATE TABLE IF NOT EXISTS `ModleDaten` (
`dataId` int(11) NOT NULL,
  `ModelId` int(11) NOT NULL,
  `Key` text NOT NULL,
  `Value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `Benutzer`
--
ALTER TABLE `Benutzer`
 ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `Models`
--
ALTER TABLE `Models`
 ADD PRIMARY KEY (`modelId`);

--
-- Indizes für die Tabelle `ModleDaten`
--
ALTER TABLE `ModleDaten`
 ADD PRIMARY KEY (`dataId`), ADD KEY `ModelId` (`ModelId`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `Benutzer`
--
ALTER TABLE `Benutzer`
MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `Models`
--
ALTER TABLE `Models`
MODIFY `modelId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT für Tabelle `ModleDaten`
--
ALTER TABLE `ModleDaten`
MODIFY `dataId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `ModleDaten`
--
ALTER TABLE `ModleDaten`
ADD CONSTRAINT `ModleDaten_ibfk_1` FOREIGN KEY (`ModelId`) REFERENCES `Models` (`modelId`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
