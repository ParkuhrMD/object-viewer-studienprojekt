-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 16. Feb 2017 um 18:13
-- Server Version: 5.6.35-log
-- PHP-Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `project_objectviewer`
--
CREATE DATABASE IF NOT EXISTS `project_objectviewer` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `project_objectviewer`;

--
-- Daten für Tabelle `Benutzer`
--

INSERT INTO `Benutzer` (`id`, `nutzername`, `password`, `acceskey`, `lastlogin`) VALUES
(0, 'a', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', 0, 1487258816),
(1, 'Tester', '9264a677ddfa1b311d09c5d2427b23a3a2a79015', NULL, 0),
(2, 'Gast1', '6d17b36eac705de95ff0ab02a6e5746fc766924a', NULL, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
