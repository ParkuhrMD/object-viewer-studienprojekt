<?php
	
	// Debug command
	error_reporting(-1);
	ini_set('display_errors', 'ON');

	define("main", "true");

	require_once "classes/mysql.class.php";

	//config
	$server = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") .$_SERVER["HTTP_HOST"];
	
	$modelsDir = "./models"; // Model folder
	$allowFileTyps = array("obj", "mtl", "png", "PNG", "xml"); // Allow file typs for Upload 

	$timeout = 2592000; //Timeout Autologin in Seconds

	$mysql_ip       = "localhost"; 			// Host/IP to MySQL Server (Importen Note: The Server should be "localhost" it's better for Security)
	$mysql_username = "pro_objectviewer"; 	// User for the MySQL Server
	$mysql_password = "nope"; 	// PW for the MySQL Server
	$mysql_database = "project_objectviewer"; // Database from MySQL Connection

	$thingworxUrl = "http://localhost:3838/Thingworx/Resources/";
	$thingworxDataServic = "/Services/GetBlogEntriesWithComments";
	$thingworxKey = "f687c8ac-bc60-4bbe-8a33-943d4c9589d8";

	//system variables
	$mysql = Database::getInstance($mysql_ip, $mysql_username, $mysql_password, $mysql_database);

	// Session starten (Wenn man den Session Namen ändert bitte auch in den Ajax Seiten, sonst können Fehler auftreten!)
	session_name("ObjectViewer");
	session_start();

	function zufallscode($stellen)
	{
		$code = '';
		$keys = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
		for ($i = 1; $i <= $stellen; $i++) {
			$zufall = mt_rand(0, count($keys) - 1);
			$code .= $keys[$zufall];
		}
		return $code;
	}

	function deleteDir($dirPath) {
	    if (! is_dir($dirPath)) {
	        throw new InvalidArgumentException("$dirPath must be a directory");
	    }
	    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
	        $dirPath .= '/';
	    }
	    $files = glob($dirPath . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_dir($file)) {
	            self::deleteDir($file);
	        } else {
	            unlink($file);
	        }
	    }
	    rmdir($dirPath);
	    return true;
	}
?>