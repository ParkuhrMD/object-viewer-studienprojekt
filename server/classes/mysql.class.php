<?php
defined("main") or die ("<html><head><title>ACCESS DENIED</title></head><body><h1>ACCESS DENIED</h1></body></html>");

class Database extends MySQLi {
     private static $instance = null ;
     private $shopdata = null;

     private function __construct($host, $user, $password, $database){
        parent::__construct($host, $user, $password, $database);

         //Shop-Settings bekommen
        $this->shopdata = array();
        $sql = "SELECT * FROM `".$database."`.`Data` WHERE `name` = 'noreply_email' OR `name` = 'debug_email' OR `name` = 'page_name'";
        $result = parent::query($sql);
        if($result != false)
            while ($row = $result->fetch_assoc()){
                $this->shopdata[$row["name"]] = $row["wert"];
            }
     }

    public function __destruct() {
        parent::close();
    }

    public static function getInstance($host, $user, $password, $database){
        if (self::$instance == null){
            self::$instance = new self($host, $user, $password, $database);
        }
        return self::$instance;
    }

    public function query($sql){   
        $starttime = microtime(true);     
        $result = parent::query($sql); 
        $endtime = microtime(true);
        $duration = $endtime - $starttime;
        //Send Querys if Response Time over 5 Sec
        if($duration > 5) {
            /* $mail = new PHPMailer;
            $mail->CharSet = 'UTF-8';

            $mail->setFrom($this->shopdata["noreply_email"], 'MySQL - Error Reporter');
            $mail->addAddress($this->shopdata["debug_email"], "MySQL - Error Reporter");
            $mail->isHTML(true);

            $mail->Subject = $this->shopdata["page_name"].' - MySQL Error Debug (Long-Response Time)';
            $mail->Body = "<pre>".$duration."<br />".var_export(debug_backtrace(),true)."</pre>"; 
            $mail->send(); */
        }    
        //Send Error Querys
        if(!$result || self::$instance->error) {
           /* $mail = new PHPMailer;
            $mail->CharSet = 'UTF-8';

            $mail->setFrom($this->shopdata["noreply_email"], 'MySQL - Error Reporter');
            $mail->addAddress($this->shopdata["debug_email"], "MySQL - Error Reporter");
            $mail->isHTML(true);

            $mail->Subject = $this->shopdata["page_name"].' - MySQL Error Debug';
            $mail->Body = "<pre>".var_export(self::$instance->error,true)."<br />".$sql."<br />".var_export(debug_backtrace(),true)."</pre>"; 
            $mail->send(); */
        }        
        return $result; 
         
    } 
}
?>