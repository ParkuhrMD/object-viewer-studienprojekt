<?php 

	require_once "classes/util.php";

	$jsonResult = array();

	// angular js param work a round
	$params = json_decode(file_get_contents('php://input'),true);

	if(!isset($_SESSION["angemeldet"])){

		$data = null;

		if (isset($params["username"]) && isset($params["password"])) {

			$userES 	= $mysql->real_escape_string($params["username"]);
			$passwordES = $mysql->real_escape_string($params["password"]);

			$passwordES = sha1($passwordES);

			$sql = "SELECT * FROM `".$mysql_database."`.`Benutzer` WHERE `nutzername` = '".$userES."' AND `password` = '".$passwordES."'";
    		$result = $mysql->query($sql);

    		if ($result->num_rows == 1) {
    			//Nur wenn EIN user existiert
    			$data = $result->fetch_assoc();

    			$newAccesKey = zufallscode(64);

				if(isset($_POST["autologin"]) && $_POST["autologin"] == "true"){
					setcookie("userautologin", $newAccesKey, time()+$timeout);
				}

				$sql = "UPDATE `".$mysql_database."`.`Benutzer` SET `acceskey`= '".$newAccesKey."' ,`lastlogin` = UNIX_TIMESTAMP() WHERE `id` = '".$data["id"]."'";
    			$result = $mysql->query($sql);

    		} else {
				$jsonResult["Error"][] = "Nutzerdaten falsch : username + password";
			}

		} else if (!empty($_COOKIE['userautologin'])) {

			$accesKeyES = $mysql->real_escape_string($_COOKIE['userautologin']);

			$sql = "SELECT * FROM `".$mysql_database."`.`Benutzer` WHERE `acceskey` = '".$accesKeyES."' AND `lastlogin` > '(UNIX_TIMESTAMP() -".$timeout.")'";
    		$result = $mysql->query($sql);

    		if ($result->num_rows == 1) {
    			//Nur wenn EIN user existiert
    			$data = $result->fetch_assoc();
    		} else {
    			if($_COOKIE["userautologin"]) {
					//Autologin Cookie löschen
					unset($_COOKIE["userautologin"]);
					setcookie("userautologin", '', time() - 3600);
				}
				$jsonResult["Error"][] = "Nutzerdaten falsch : userautologin";	
    		}

		}

		if($data != null){
			$_SESSION["angemeldet"] = true;
			//Public Userdata!!!
		    $_SESSION["userData"] = array(
		    	"id" => $data["id"],
		        "Username" => $data["nutzername"]
		    );
		} else {
			$jsonResult["Error"][] = "Keine Nutzerdaten gefunden / Autologin";
		}
	}

	if($_SESSION["userData"] != null){
		$jsonResult["userData"] = $_SESSION["userData"];
	} else {
		$jsonResult["Error"][] = "Keine Nutzerdaten";
	}

	echo json_encode($jsonResult);

	/*if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' ) {
    	echo '<meta http-equiv="refresh" content="0; URL=index.html">';
	}*/

?>