var ovModule = angular.module('objViwer', []);

ovModule.controller('objViwerController', ['$scope','$http', function($scope,$http) {
  var objViwer = this;

  // Client Data -> Linkt with UI
  objViwer.models = null;

  objViwer.init = function(){
    objViwer.models = null;
    objViwer.loadModels();
  };

  objViwer.loadModels = function() {
    $http({
        method: 'POST',
        url: './getModelList.php',
        data: {},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        if(response.status == 200)
          objViwer.models = response.data;
        Object.keys(objViwer.models).forEach(function (key) {
            // do something with obj[key]
            objViwer.models[key]["dName"] = true;
        });
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        console.log(response);
    });
  };

  objViwer.saveName = function(model){
    $http({
        method: 'GET',
        url: "./setModelName.php",
        params : { modelId : model.DB.modelId , newName : model.DB.modelName},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function successCallback(response) {
        if(response.status == 200 && response.data.DB != null){
          model.dName = true;
        } else {
          console.log(response);
        }
      }, function errorCallback(response) {
        console.log(response);
      });
  }

  objViwer.generateNewAccesKey = function(model) {
    $http({
        method: 'GET',
        url: "./freigabe.php",
        params : { action : "newKey" , modelId : model.DB.modelId},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function successCallback(response) {
        if(response.status == 200 && response.data.NewCode != null){
          model.DB.modelAcceskey = response.data.NewCode;
        }
      }, function errorCallback(response) {
        console.log(response);
      });
  }

  objViwer.deletAccesKey = function(model) {
    $http({
        method: 'GET',
        url: "./freigabe.php",
        params : { action : "deletKey" , modelId : model.DB.modelId},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then(function successCallback(response) {
        if(response.status == 200){
          model.DB.modelAcceskey = null;
        }
      }, function errorCallback(response) {
        console.log(response);
      });
  }

  objViwer.deletModel = function(model){
    bootbox.confirm({
      message: "Das Model wirklich Löschen?",
      buttons: {
          confirm: {
              label: 'Ja',
              className: 'btn-success'
          },
          cancel: {
              label: 'Nein',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        if(result){
          $http({
              method: 'GET',
              url: "./deleteModel.php",
              params : { modelId : model.DB.modelId },
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).then(function successCallback(response) {
              if(response.status == 200 && response.data.error == null && response.data.modelId != null){
                delete objViwer.models[response.data.modelId]
              } else {
                console.log("Fehler beim Löschen Admin kontaktieren");
                console.log(response);
              }
            }, function errorCallback(response) {
              console.log(response);
            });
        }
      }
    });
  }

  objViwer.deletFile = function(model, file){
    bootbox.confirm({
      message: "Das Datei wirklich Löschen?",
      buttons: {
          confirm: {
              label: 'Ja',
              className: 'btn-success'
          },
          cancel: {
              label: 'Nein',
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        if(result){
          $http({
              method: 'GET',
              url: "./deleteFile.php",
              params : { modelId : model.DB.modelId , filename : file },
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).then(function successCallback(response) {
              if(response.status == 200 && response.data.erfolg != null){
                var found = null;
                for (var i = 0; i < model.Files.length; i++) {
                  var myFile = model.Files[i];
                  if(myFile == file){
                    found = i;
                  }
                }
                if(found != null)
                  model.Files.splice(found, 1);
              } else {
                console.log("Fehler beim Löschen Admin kontaktieren");
              }
              console.log(response);
            }, function errorCallback(response) {
              console.log(response);
            });
        }//END IF Result
      }
    });
  }

  $scope.$on('login', function (event, arg) {
    objViwer.init();
  });

  objViwer.init(); 
}]);

/* ****************************************************************************************************** */

ovModule.controller('loginController', ['$rootScope','$scope','$http', function($rootScope,$scope,$http) {
  var loginCon = this;

  loginCon.userData = null;

  loginCon.init = function(){
    loginCon.login({});
  };

  loginCon.login = function(formData) {
    $http.post("./login.php", formData)
      .then(function successCallback(response) {
        if(response.status == 200 && response.data.userData != null){
          loginCon.userData = response.data.userData;
          $rootScope.$broadcast('login', true);
        }
        if(response.status == 200 && response.data.Error != null)
          console.log(response.data.Error);
      }, function errorCallback(response) {
        console.log(response);
      });

  };

  loginCon.logout = function(formData) {
    $http.post("./logout.php", formData)
      .then(function successCallback(response) {
        loginCon.userData = null;
        $rootScope.$broadcast('login', true);
      }, function errorCallback(response) {
        console.log(response);
      });
  };

  loginCon.loginWithData = function() {
    formData = { 
        username : loginCon.username , 
        password : loginCon.userpassword , 
        autologin : loginCon.autologin
      };
    loginCon.login(formData);
  };


  loginCon.init(); 

}]);