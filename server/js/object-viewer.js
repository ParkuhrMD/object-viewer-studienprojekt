var scene, camera, renderer, controls, manager;
var objectGroup;
var materialDefault, materialSelected, materialsLoaded;

var globalScale = 1;
var sidebarWidth = 400;
var padding = 4;

var width, height;
var raycaster, mousePos, isDragging;
var currentSelectedObject;
var textBoxList = [];

var treeStructure;

var getDataFun;
var kompCache = [];

/** 
*  Initializes the 3D-View and adds it to the HTML-Context
*  @param {String} url - The URL to the folder containing the model
*  @param {String} file - The name of the file to load inside the given URL. 
*    The name should be given without a file extension. OBJ and MTL should have the same name.
*/
function init(url,file, dataFun) {

    getDataFun = dataFun;

    scene = new THREE.Scene();

    var container = document.getElementById( 'container' );

    width = window.innerWidth-padding - sidebarWidth;
    height = window.innerHeight-padding;

    // Set background color
    scene.background = new THREE.Color(0xdddddd);

    // Create perspective camera
    camera = new THREE.PerspectiveCamera( 75, width / height, 1, 10000 );
    camera.position.z = 1000;
    camera.position.y = 1000;

    // Set up default materials
    materialDefault = new THREE.MeshLambertMaterial( { color: 0xaaaaaa } );
    materialSelected = new THREE.MeshLambertMaterial( { color: 0xaa0000 } );

    // Set up lights
    var ambient = new THREE.AmbientLight( 0x101010 );
    scene.add( ambient );

    var directionalLight = new THREE.PointLight( 0xffffff, 0.8, 5000 );
    directionalLight.position.set( 1000, 1000, 1000 );
    scene.add( directionalLight );

    var hemiLight = new THREE.HemisphereLight(0xffffff, 0x080808, 0.4);
    scene.add(hemiLight);


    // Create new renderer with given size
    renderer = new THREE.WebGLRenderer({'antialias': true});
    renderer.setSize( width, height);

    // Add display to the html-page
    // var container = document.getElementById( 'container' );
    //var container = document.body;
    container.appendChild( renderer.domElement );


    // Initialize camera controls
    controls = new THREE.OrbitControls( camera, renderer.domElement );
    //controls.addEventListener( 'change', render ); // add this only if there is no animation loop (requestAnimationFrame)
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.enableZoom = true;


    manager = new THREE.LoadingManager();
    manager.onProgress = function ( item, loaded, total ) {

        console.log( item, loaded, total );

    };

    loadOBJwithMTL(url,file);

    // Add Buttons to html-page
    var exitButton = document.getElementById('exitButton');
    exitButton.style.visibility = 'visible';
    exitButton.style.left= width - 50 +'px';
    exitButton.style.top= height - 25 +'px';

    var refreshButton = document.getElementById('refreshButton');
    refreshButton.style.visibility = 'visible';
    refreshButton.style.left= width - 125 +'px';
    refreshButton.style.top= height - 25 +'px';

    controls.addEventListener('change', updateAllTextBoxes);

    window.addEventListener('mousedown', onMouseDown, false);
    window.addEventListener('mousemove', onMouseDrag, false);
    window.addEventListener('mouseup', onMouseUp, false);
    raycaster = new THREE.Raycaster();
    mousePos = new THREE.Vector2();

}

// Camera Animation Variables
var camAnim = false, camAnimTarget, camAnimSource, camFinalTarget, camAnimProgress = 0.0;

/**
 * Animation function used by three.js
 */
function animate() {

    requestAnimationFrame( animate );

    // Animate camera if camera is moving right now
    if (camAnim) {
        if (camAnimProgress > 1) {
            camAnim = false;
            camAnimProgress = 1.0;
            // Update controls after camera movement is finished
            controls.target = camFinalTarget;
            controls.update();
        }
        camera.position.copy(interpolate(camAnimSource, camAnimTarget, camAnimProgress))
        camAnimProgress += 0.02;
    }

    renderer.render( scene, camera );

}

function onMouseDown(event) {
    isDragging = false;
    mousePos.x = event.screenX;
    mousePos.y = event.screenY;
}

function onMouseDrag(event) {
    if (isDragging)
        return
    if ((event.screenX - mousePos.x)*(event.screenX - mousePos.x) + (event.screenY - mousePos.y)*(event.screenY - mousePos.y) > 10)
        isDragging = true;
}

function onMouseUp(event) {
    // Continue only if the left mouse button is pressed and we are not dragging
    if(event.button != 0 || isDragging)
        return

    var projectionWidth = window.innerWidth-padding-sidebarWidth;

    if(event.clientX > projectionWidth) {
        //Mouse up on tree view?
        return
    }

    if(!(event.shiftKey || event.ctrlKey)) {
        destroyAllTextBoxes();
        deselectAllObjects();
    }

    mousePos.x = ( event.clientX / width ) * 2 - 1;
    mousePos.y = - ( event.clientY / height ) * 2 + 1;

    raycaster.setFromCamera(mousePos, camera);

    var intersect = raycaster.intersectObjects(objectGroup.children);

    if(intersect.length > 0) {
        var selectedObject = intersect[0].object;
        selectObject(selectedObject);
        requestTextBox(selectedObject);
        treeView.reloadData();
    }
}

/**
 * Interpolates two Vectors with value t
 * 
 * @param  {THREE.Vector3}
 * @param  {THREE.Vector3}
 * @param  {float}
 * @return {THREE.Vector3}
 */
function interpolate(v1, v2, t) {
    v = new THREE.Vector3();
    v.copy(v2);
    v.sub(v1);
    v.multiplyScalar(t);
    v.add(v1);
    return v;
}

/**
 * Loads an OBJ-file with its MTL-File. Runs async.
*  @param {String} url - The URL to the folder containing the model
*  @param {String} file - The name of the file to load inside the given URL. 
 */
function loadOBJwithMTL(url,file) {

    var texture = new THREE.Texture();

    var onProgress = function ( xhr ) {
        if ( xhr.lengthComputable ) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            console.log( Math.round(percentComplete, 2) + '% downloaded' );
        }
    };

    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setPath(url);
    mtlLoader.load( file + '.mtl', function(material) {loadOBJ(url, file, material); loadTreeXML(url, file)}, onProgress, 
        function(xhr){console.log("Something went wrong while loading the material: " + xhr); loadOBJ(url, file, null);});
}

/**
 * Loads an OBJ-file and applies the given Materials
 * @param {String} url - The URL to the folder containing the model
 * @param {String} file - The name of the file to load inside the given URL. 
 * @param  {List of THREE.Material} materials - The list of loaded Materials as given by the MTL loader. Can be null to apply default Material
 */
function loadOBJ(url, file, materials ) {

    if(materials != null) {
        materials.preload();
    
        materialsLoaded = materials;
    
        // Convert to array once to be able to access later as iterator
        materialsLoaded.getAsArray();
    }

    var onProgress = function ( xhr ) {
        if ( xhr.lengthComputable ) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            console.log( Math.round(percentComplete, 2) + '% downloaded' );
        }
    };
    
    var onError = function ( xhr ) {
        console.log("Something went wrong while loading the object: " + xhr)
    };

    var objLoader = new THREE.OBJLoader();
    objLoader.setMaterials( materials );
    objLoader.setPath(url);
    objLoader.load( file + '.obj', function ( object ) {

        object.traverse( function ( child ) {

            if ( child instanceof THREE.Mesh ) {

                child.userData.originalMaterial = child.material;

            }

        } );
        scene.add( object );
        objectGroup = object;
        objectGroup.rotateX(THREE.Math.degToRad(-90))

        // Point camera at estimated center of gravity
        var c = new THREE.Vector3(0,0,0)
        object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
                child.geometry.computeBoundingBox()
                c.add(child.geometry.boundingBox.getCenter())
            }
        })
        c.multiplyScalar(1.0/objectGroup.children.length)
        controls.target = c;
        controls.update();

        if(treeView==null) {
            getName = getNameScene;
            initView(scene);
        }
    }, onProgress, onError );
}

function loadTreeXML(url, file) {
    var loader = new THREE.FileLoader();

    loader.setPath(url);
    loader.load(file + '.xml', function(data) {
        var parser = new DOMParser();
        var xmldoc = parser.parseFromString(data, "text/xml");
        initView(xmldoc);
    }, function(){}, function(xhr) {
        console.log('Could not load tree structure. Using scene structure instead');
    });
}

/**
 * Sets the material for the given object'to materialSelected
 * @param  {THREE.Object}
 */
function selectObject(object) {

    object.material = materialSelected;
    currentSelectedObject = object;
}

/**
 * Sets the material for ass objects to their original material, or to the default material if not available
 */
function deselectAllObjects() {

    scene.traverse( function ( child ) {
        if ( child instanceof THREE.Mesh ) {
            child.material = child.userData.originalMaterial || materialDefault;
        }
    });
    currentSelectedObject = null;
}

/**
 * Makes all loaded materials transparent
 */
function enableTransparency() {
    for(var i in materialsLoaded.materialsArray) {
        materialsLoaded.materialsArray[i].transparent = true;
        materialsLoaded.materialsArray[i].opacity = 0.5;
    }
    materialDefault.transparent = true;
    materialDefault.opacity = 0.5;
}

/**
 * Makes all loaded materials opaque
 */
function disableTransparency() {
    for(var i in materialsLoaded.materialsArray) {
        materialsLoaded.materialsArray[i].transparent = false;
    }
    materialDefault.transparent = false;
}

/**
 * Focuses the camera on a specific object.
 * The camera rotation is maintained, but the position and control target are modified so that the frustrum fits the bounding box.
 * @param {THREE.Object} object - The object to be focused
 * @param {bool} anim - If true, the camera will fly towards the new position. If false, it is instantly set.
 */

function focusObject(object, anim = true) {
    if (object.geometry.boundingBox == null) {}
    object.geometry.computeBoundingBox();

    boundingBox = object.geometry.boundingBox;

    p1 = boundingBox.min;
    p2 = boundingBox.max;

    objCenter = object.localToWorld(boundingBox.getCenter());

    s = p1.distanceTo(p2);
    fov = THREE.Math.degToRad(camera.getEffectiveFOV());
    d = (s/2) / Math.tan(fov/2);

    var cameraVector = new THREE.Vector3(0,0,1);
    cameraVector.applyQuaternion(camera.quaternion)
    console.log(cameraVector);
    cameraVector.multiplyScalar(d);

    cameraVector.add(objCenter)
    if(anim){
        camAnim = true;
        camAnimSource = camera.position;
        camAnimTarget = cameraVector;
        camFinalTarget = objCenter;
        camAnimProgress = 0.0;
    } else {
        camera.position.copy(cameraVector);
        controls.target = objCenter;
        controls.update();
    }

}

/**
 * Calculates the screen position in Pixels for a given vector
 * @param  {THREE.Vector3} pos - The position in 3D space
 * @return {THERE.Vector3} The position in screen space from the top left
 */
function toXYCoords (pos) {
    var vector = pos.clone().project(camera);
    vector.x = (vector.x + 1)/2 * renderer.getSize().width;
    vector.y = -(vector.y - 1)/2 * renderer.getSize().height;
    return vector;
}

/**
 * @constructor
 * @param {String} text - The text that the TextBox should display
 * @param {THREE.Object} object - The object this TextBox should be applied to
 */
function TextBox (text, object) {
	var overlay;
	this.overlay = overlay;
	this.boxText = text;
	this.myObject = object;
	this.id = this.myObject.name
	this.overlay = document.createElement('div');
	this.overlay.id = this.id;
    this.overlay.className = "infobox";
    this.overlay.innerHTML = this.boxText;

    var objBoundingBox = this.myObject.geometry.boundingBox;
    var center = this.myObject.localToWorld(objBoundingBox.getCenter());
    var centerVec = toXYCoords(center);

    document.body.appendChild(this.overlay);

    this.update();

}

/**
 * Update the location of this TextBox. TextBoxes are placed depending on the bounding box of the object in screen space
 */
TextBox.prototype.update = function() {

	var objBoundingBox = this.myObject.geometry.boundingBox;
    // var center = this.myObject.localToWorld(objBoundingBox.getCenter());
    var maxBB = this.myObject.localToWorld(objBoundingBox.max.clone());
    var minBB = this.myObject.localToWorld(objBoundingBox.min.clone());
    var boxVertices = [
        toXYCoords(new THREE.Vector3(maxBB.x, maxBB.y, maxBB.z)),
        toXYCoords(new THREE.Vector3(maxBB.x, maxBB.y, minBB.z)),
        toXYCoords(new THREE.Vector3(maxBB.x, minBB.y, maxBB.z)),
        toXYCoords(new THREE.Vector3(maxBB.x, minBB.y, minBB.z)),
        toXYCoords(new THREE.Vector3(minBB.x, maxBB.y, maxBB.z)),
        toXYCoords(new THREE.Vector3(minBB.x, maxBB.y, minBB.z)),
        toXYCoords(new THREE.Vector3(minBB.x, minBB.y, maxBB.z)),
        toXYCoords(new THREE.Vector3(minBB.x, minBB.y, minBB.z)),
    ];

    var maxX = Number.NEGATIVE_INFINITY;
    var maxY = Number.NEGATIVE_INFINITY;
    var maxZ = Number.NEGATIVE_INFINITY;
    var tmp;
    for (var i=7; i>=0; i--) {
        tmp = boxVertices[i];
        if (tmp.x > maxX) maxX = tmp.x;
        if (tmp.y > maxY) maxY = tmp.y;
        if (tmp.z > maxZ) maxZ = tmp.z;
    }
    var centerVec = new THREE.Vector3(maxX, maxY, maxZ);

    // If point is behind the camera, dont draw it
    if(centerVec.z < 0)
        this.overlay.style.visibility = 'hidden';
    else
        this.overlay.style.visibility = 'visible';

    // Constrain the div to the size of the renderer
    var clientRect = this.overlay.getBoundingClientRect()
    if(centerVec.x + clientRect.width > width || centerVec.x < 0 || centerVec.y + clientRect.height > height || centerVec.y < 0) {
        centerVec.x = Math.min(centerVec.x, width - clientRect.width);
        centerVec.y = Math.min(centerVec.y, height - clientRect.height);
        centerVec.x = Math.max(centerVec.x, 0);
        centerVec.y = Math.max(centerVec.y, 0);
    }

    this.overlay.style.top = centerVec.y + 'px';
    this.overlay.style.left = centerVec.x + 'px';
    
}


/**
 * Deletes a TextBox and removes it from the list of text boxes
 * @param  {TextBox} textBox - The textBox to delete
 */
function destroyTextBox (textBox) {
	var elem = document.getElementById(textBox.id)
	elem.outerHTML = "";
	delete elem;

	var index = textBoxList.indexOf(textBox);
	textBoxList.splice(index, 1);

	delete textBox.overlay;
	delete textBox.id;
	delete textBox.boxText;
	delete textBox.myObject;

}

/**
 * Destroys all TextBoxes currently active
 */
function destroyAllTextBoxes() {
    for(var i = textBoxList.length-1; i >= 0; i--) {
        destroyTextBox(textBoxList[i]);
    }
}

/**
 * Updates the Position of all TextBoxes currently active
 */
function updateAllTextBoxes() {
	for (i=0 ; i < textBoxList.length; i++) {
		textBoxList[i].update();
	}
}

//convertiert übergebene json in String Tabelle der Werte des Json, gibt den formatierten String zurück
function convertJson(json){
	var text ="";
	var lib = JSON.parse(json);
	var jsonKeys = Object.keys(lib);
	jsonKeys.sort();
	for (i = 0; i < jsonKeys.length; i++){
		text += jsonKeys[i] + ": "+ lib[jsonKeys[i]] + " <br>";
	}
	return text
}

//convertiert übergebene Objekt in String Tabelle der Werte des Objekt, gibt den formatierten String zurück
function convertObj(obj){
    var text ="";
    var objKeys = Object.keys(obj);
    objKeys.sort();
    for (i = 0; i < objKeys.length; i++){
        text += objKeys[i] + ": "+ obj[objKeys[i]] + " <br>";
    }
    return text
}

//erwartet json-datei und Object Name als String! Erstellt Textbox für Object mit dem Object Namen als ID und trägt ID in die textBoxList ein.
function createTextBox(objekt, objectName){
	this.id = objectName;
	this.obj = scene.getObjectByName(objectName);
	//this.text = convertJson(json);
    this.text = convertObj(objekt);
	objectName = new TextBox(this.text, this.obj);
	textBoxList.push(objectName);

}

var hint = { Data : "Not Available!" };

// Starts an ajax-Request to read the json from the server and displays it as a TextBox
function requestTextBox(object) {
    if(kompCache[object.name] == null){
        if(getDataFun instanceof Function){
            getDataFun(object.name , function( respons ) {
                console.log(respons);
                if(respons.Data == null){
                    createTextBox(hint, object.name);
                } else {
                    kompCache[object.name] = respons.Data;
                    createTextBox(respons.Data, object.name); 
                }
            });
        }
    } else {
        createTextBox(kompCache[object.name], object.name);
    }
}

/**
* Clear Cache of TextBox Inforamtions.
*/
function clearBoxCache(){
    kompCache = [];
}

/**
 * Resizes the renderer ańd camera to fit the container and repositions the buttons
 */
function resizeContent(){
    var container = document.getElementById( 'container' );
    var width = window.innerWidth-padding-sidebarWidth;
    var height = window.innerHeight-padding;
    renderer.setSize( width, height);
    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    exitButton.style.left= width - 50 +'px';
    exitButton.style.top= height - 25 +'px';

    refreshButton.style.left= width - 125 +'px';
    refreshButton.style.top= height - 25 +'px';
}
