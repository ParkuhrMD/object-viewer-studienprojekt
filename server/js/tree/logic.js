/**
 * Created by jan-niklasfreundt on 25/10/16.
 */

var treeData = null;
var treeFilteredData = null;
var treeView = null;

function initView() {
    console.log("logic init");

    this.treeData = new TreeData("a");
    console.log("tree data" + treeData.getName());
    var b = new TreeData("b");
    treeData.addChild(b);
    var c = new TreeData("c");
    treeData.addChild(c);
    var d = new TreeData("d");
    b.addChild(d);
    var e = new TreeData("Kekse");
    b.addChild(e);
    var b2 = new TreeData("b");
    treeData.addChild(b2);
    var b3 = new TreeData("b");
    treeData.addChild(b3);
//    treeView.reloadData();

    console.log("tree data children", treeData.getNumberOfChildren());
    console.log("b: ",b.getName());
   // console.log("c: ",c.getName());
    console.log("d: ",treeData.getChild(0));
    treeView = new TreeView("treeView", this, this.document, true);
    treeView.reloadData();
}

function treeViewNumberOfElements(treeView, treeElement) {
    if(treeElement == null) {
        return 1;
    }

    return treeElement.getNumberOfChildren();
}

function treeViewGetElementForIndex(treeView2, parent, index) {
    if(parent == null) {
        console.log("treeview filter", this.treeView.isFiltering());

        if(this.treeView.isFiltering() == false) {
            return treeData;
        } else {
            return treeFilteredData;
        }
    }
    return parent.getChild(index);
}

function treeViewGetDOMForElement(treeView, element, layer) {
    var div = document.createElement("div");

    /*var layerString = "";
     for(var i=0; i<layer; i++) {
     layerString = "-"+layerString;
     }*/

    var button = document.createElement("button");
    //button.style.backgroundColor = 'blue';

    if(element.childrenHidden) {
        button.innerHTML = " + ";

    } else {
        button.innerHTML = " - ";

    }
    button.style.marginTop = "15px";
    button.style.float = 'right';
    button.representation = element;
    button.addEventListener("click", function(sender) {
        //delegate.treeViewClickedElement()

        if(element.childrenHidden) {
            element.childrenHidden = false;
        } else {
            element.childrenHidden = true;
        }
       // treeView.reloadData();
    })

    div.style.marginLeft = (layer*20)+"px";

    var span = document.createElement("span");
    //span.style.verticalAlign = 'middle';
    span.innerHTML = element.getName();
    span.style.marginTop = '15px';
    div.appendChild(span);

    div.classList.add("treeCell");
  //  console.log("domElement:", div);
    if(element.hasChildren()) {
        div.appendChild(button);
    }

    return div
}

function treeViewClickedElement(treeView2, element) {
    console.log("clicked element: "+element.getName());
    document.getElementById("clickedDiv").innerHTML="Clicked:" + element.getName();

    var b = new TreeData("b");
    this.treeData.addChild(b);
    console.log("this.treeView:", this.treeView);
    treeView.reloadData();
}

function treeViewSearchFor(treeView2,searchString) {

    console.log('search for ', searchString);

    console.log('filter enabled ', this.treeView.isFiltering());
    this.treeFilteredData = getFilteredDataFor(this.treeData,searchString)[0];
    console.log("RESULT",treeFilteredData);
    treeView.reloadData();
}

function getFilteredDataFor(treeElement, filter, f) {
    var count = treeElement.getNumberOfChildren();
    var newElement = new TreeData(treeElement.getName());

    var found = f

    if(treeElement.hasChildren() == false) {
        if (newElement.getName().toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
            console.log(newElement.getName(), 'includes', filter);
            found = true;
        } else {
            return [null, false];
        }
    } else {
        for (var i = 0; i < count; i++) {
            var subelement = treeElement.getChild(i);

            if (subelement.getName().toLocaleLowerCase().indexOf(filter.toLowerCase()) !== -1) {
                found = true;
            }

            var returnValue = getFilteredDataFor(subelement,filter, found);
            var ele = returnValue[0];

            if(returnValue[1] == true) {
                found = true;
            }

            if(found && ele != null) {
                newElement.addChild(ele);
            }
        }
    }

    return [newElement,found];
    //return null;
}
