/**
 * Created by jan-niklasfreundt on 25/10/16.
 */
TreeData = function(name) {
    this.name = name;
    this.children  = new Array();
    this.childrenHidden = false; // required
    
    this.addChild = function(name) {
        var child = (name);
        this.children.push(child);
    }

    this.getNumberOfChildren = function() {
        return this.children.length;
    }
    
    this.hasChildren = function() {
        return (this.children.length > 0);
    }

    this.getChild = function(i) {
        var child = this.children[i];
        return child;
    }

    this.getName = function() {
        return this.name;
    }
}