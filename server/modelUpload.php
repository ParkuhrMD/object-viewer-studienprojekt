<?php

require_once "classes/util.php";

if($_SESSION["angemeldet"]){ 

    $return = [];

    $keepServerFile = true;
    $newModelName = "NewModel_".time();

    if(isset($_POST["clientPriority"]) && $_POST["clientPriority"] == "true"){
        $keepServerFile = false;
    }

    if(isset($_POST["modelName"]) && $_POST["modelName"] != null){
         $newModelName = $_POST["modelName"];
    }

    $target_dir = $modelsDir."/".$_SESSION["userData"]["id"]."/";
    $id = null;

    // Überprüfung ob es da Model gibt
    if(isset($_POST["modelId"]) && $_POST["modelId"] != null){

        $sql = "SELECT * FROM `".$mysql_database."`.`Models` WHERE `modelId` = '".$_POST["modelId"]."';";
        $result = $mysql->query($sql);

        //Nur ein Model mit der Id solte existiert
        if ($result && $result->num_rows == 1) {
            $data = $result->fetch_assoc();
            $id = $data["modelId"];
        } else {
            $return["Error"] = " Parameter : modelId => keine Model in der Datenbank gefunden!"; 
        }

    // Ein neues Model anlegen
    } else {
        // New Model
        $sql = "INSERT INTO `".$mysql_database."`.`Models` (`modelId`, `modelName`, `modelOwner`, `modelAcceskey`) VALUES (NULL, '".$newModelName."', '".$_SESSION["userData"]["id"]."', NULL);";
        $result = $mysql->query($sql);
        if ($result && $mysql->affected_rows > 0) {
            $id = $mysql->insert_id;
        }else{
            $return["Error"] = " New Model => Es konnte kein neued Model in der Datenbank erstellt werden!";  
        }
    }

    $lastFileObj = null;
    $lastFileMtl = null;

    // Die Verarbeitung der Files beginnen
    if ($id != null){ 
        // Das Verzeichniss vorbereiten
        $target_dir .= $id."/";
        if(!is_dir($target_dir)) {
            mkdir($target_dir,0777,true);
        }

        // Alle ankommenden Files verarbeiten
        foreach ($_FILES['fileToUpload']['name'] as $index => $name) {
            $baseFileName = basename($_FILES["fileToUpload"]["name"][$index]);
            $target_file = $target_dir . $baseFileName;
            $fileStatusOk = true;
            $filePathInfo =  pathinfo($name);
            $fileType = $filePathInfo["extension"];
            $fileName = $filePathInfo["filename"];

            // Überprüft ob File vorhanden ist und behalten werden soll.
            if ($keepServerFile && file_exists($target_file)) {
                $return["Files"][$baseFileName]["Error"][] = "File : '".$name."' ist schone auf dem Server vorhanden!";
                $fileStatusOk = false;
            }

            // Allow certain file formats
            if(!in_array($fileType, $allowFileTyps)) {
                $return["Files"][$baseFileName]["Error"][] = "Filetype : '.".$fileType."'' wird vom Server nicht unterstützt!";
                $fileStatusOk = false;
            }

            // Überprüft ob File Status okey ist 
            if ($fileStatusOk == false) {
                // File ist keine erlaubter Upload. 
            // Wenn ok, wird versucht die File hochzuladen
            } else {
                // File wird verschoben / upgeloaded
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$index], $target_file)) {
                    // Erfolgreich verschoben / upgeloaded
                    $return["Files"][$baseFileName]["Erfolg"][] = "File : '".$baseFileName."' wurde Erfolgreich hochgeladen!";
                    // Merken der letzten obj Datei als Main file für datenback
                    if($fileType == "obj")
                        $lastFileObj = $fileName;
                    if($fileType == "mtl")
                        $lastFileMtl = $fileName;
                } else {
                    $return["Files"][$baseFileName]["Error"][] = "File : '".$baseFileName."' konte nicht auf dem Server verschoben werden!";
                }
            }
        }
    
        // Wenn eine Obj File gefunden wurde 
        $lastFile = null;
        if($lastFileMtl != null){
            $lastFile = $lastFileMtl;
        } else {
            if($lastFileObj != null)
                $lastFile = $lastFileObj;
        }

        if($lastFile != null){
            // Aktualisirung Database  
            $sql = "UPDATE `".$mysql_database."`.`Models` SET `modelName` = '".$lastFile."' WHERE `Models`.`modelId` = '".$id."';";
            $result = $mysql->query($sql);
            if ($result == 1 && $mysql->affected_rows > 0) {
                $return["Database"]["Erfolg"] = "Datenbank erfolgreich Aktualisirung!";
            }else{
                $return["Database"]["Error"] = "Datenbank konnte nicht Aktualisirung werden!";
            }
        }
    }
    echo json_encode($return);
}

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' ) {
    echo '<meta http-equiv="refresh" content="0; URL=index.html">';
} 

?>
