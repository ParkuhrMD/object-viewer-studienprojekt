<?php

require_once "classes/util.php";

if($_SESSION["angemeldet"]){ 

	$return = [];

	//Check Database
	$sql = "SELECT * FROM `".$mysql_database."`.`Models` WHERE `modelOwner` = '".$_SESSION["userData"]["id"]."';";
    $result = $mysql->query($sql);

    while ($row = $result->fetch_assoc()) {
    	$return[$row["modelId"]]["DB"] = $row;
    }

    $result->free();

    //Check File system
    
	$files = fillArrayWithFileNodes( new DirectoryIterator( $modelsDir."/".$_SESSION["userData"]["id"]."/" ) );
	foreach ( $files as $index => $filelist ){
		$return[$index]["Files"] = $filelist;
	}

	echo json_encode($return);

} else {
    http_response_code(401); //Unauthorized
    die("Error: Ungültiger Nutzer.");
}


function fillArrayWithFileNodes( DirectoryIterator $dir )
	{
	  $data = array();
	  foreach ( $dir as $node )
	  {
	    if ( $node->isDir() && !$node->isDot() )
	    {
	      $data[$node->getFilename()] = fillArrayWithFileNodes( new DirectoryIterator( $node->getPathname() ) );
	    }
	    else if ( $node->isFile() )
	    {
	      $data[] = $node->getFilename();
	    }
	  }
	  return $data;
	}

?>