<?php 

	require_once "classes/util.php";
	
	$_SESSION = array();
	session_destroy();

	if($_COOKIE["userautologin"]) {
		//Autologin Cookie löschen
		unset($_COOKIE["userautologin"]);
		setcookie("userautologin", '', time() - 3600);
	}

	$return["Status"] = "Logout";

	echo json_encode($return);

	if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' ) {
    	echo '<meta http-equiv="refresh" content="0; URL=index.html">';
    }
		
?>