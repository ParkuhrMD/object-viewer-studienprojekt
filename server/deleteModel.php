<?php

require_once "classes/util.php";

if($_SESSION["angemeldet"]){

	$return = [];

	if(isset($_GET['modelId']) && !empty($_GET['modelId'])){
        
        // Check if model exist in DB
    	$sql = "SELECT * FROM `".$mysql_database."`.`Models` WHERE `modelId` = '".$_GET['modelId']."' AND `modelOwner` = '".$_SESSION["userData"]["id"]."';" ;
        $result = $mysql->query($sql);

        if ($result && $result->num_rows == 1) {

        	$row = $result->fetch_assoc();

        	// Remove all Files
        	$location = $modelsDir."/".$_SESSION["userData"]["id"]."/".$_GET['modelId']."/";
        	if(deleteDir($location)){
        		// Remove DB entry
        		$sql2 = "DELETE FROM `".$mysql_database."`.`Models` WHERE `modelId` = ".$row["modelId"].";";
        		$result2 = $mysql->query($sql2);

        		if($result2 && $mysql->affected_rows != 0){
        			$return["modelId"] = $row["modelId"];
        		} else{
        			$return["error"][] = "DB konnte nicht gelösch werden : ".$row["modelId"];
        		}
        	} else {
        		$return["error"][] = "Files konnte nicht gelösch werden : ".$row["modelId"];
        	}
        } else {
        	$return["error"][] = "Es gibt kein Model mit : ".$_GET['modelId'];
        }
    } else{
        $return["error"][] = "Parameter modelId leer oder falsch."; 
    }

    echo json_encode($return);

} else {
	http_response_code(404); //Not Found
	die("Error: File not found.");
}
?>