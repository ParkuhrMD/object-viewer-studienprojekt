<?php 

require_once "classes/util.php";

if($_SESSION["angemeldet"]){

	$return = [];

	if(isset($_GET['action']) && isset($_GET['modelId']) && !empty($_GET['modelId'])){
		if($_GET['action'] == "newKey"){

			$isNewKey = true;

			do {
				$code =  zufallscode(16);

				$sql = "SELECT `modelId` FROM `".$mysql_database."`.`Models` WHERE `modelAcceskey` = '".$code."'";
            	$result = $mysql->query($sql);

            	if ($mysql->affected_rows > 0) {
            		$isNewKey = false;
            	}

			} while (!$isNewKey);

			$sql = "UPDATE `".$mysql_database."`.`Models` SET `modelAcceskey` = '".$code."' WHERE `Models`.`modelId` = '".$_GET['modelId']."';";

            $result = $mysql->query($sql);
            if ($result == 1 && $mysql->affected_rows > 0) {
                $return["Database"]["Erfolg"] = "Datenbank erfolgreich Aktualisirung!";
                $return["NewCode"] = $code;
            }else{
                $return["Database"]["Error"] = "Datenbank konnte nicht Aktualisirung werden!";
            }

		} else if ($_GET['action'] == "deletKey"){

			$sql = "UPDATE `".$mysql_database."`.`Models` SET `modelAcceskey` = NULL WHERE `Models`.`modelId` = '".$_GET['modelId']."';";
            $result = $mysql->query($sql);
            
            if ($result == 1 && $mysql->affected_rows > 0) {
                $return["Database"]["Erfolg"] = "Datenbank erfolgreich Aktualisirung!";
            }else{
                $return["Database"]["Error"] = "Datenbank konnte nicht Aktualisirung werden!";
            }

		}
	}

	echo json_encode($return);

} else {
    http_response_code(401); //Unauthorized
    die("Error: Ungültiger Nutzer.");
}

?>