<?php 

require_once "classes/util.php";

if($_SESSION["angemeldet"]){
	http_response_code(200);
	$return = [];

	if(isset($_GET['modelId']) && !empty($_GET['modelId'])){

		//TODO fit data Strukture of the Projekt
		//$url = $thingworxUrl + $_GET['modelId'] + $thingworxDataServic;
		$url = "https://itmrub.cloud.thingworx.com/Thingworx/Things/BlueHelmet/Properties";

		$curl = curl_init($url);

		$headers = [
			'Accept: application/json',
			'Cache-Control: no-cache',
			'Content-Type: application/json; charset=utf-8',
			'Appkey: f687c8ac-bc60-4bbe-8a33-943d4c9589d8'
		];

		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$content = curl_exec( $curl );

		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    $response = curl_getinfo( $curl );

	    curl_close ( $curl ); 

	    if($code == 200){

	    	$return["Request"] = $content;

		    $sql = "INSERT INTO `".$mysql_database."`.`ModleDaten`(`ModelId`, `Key`, `Value`) VALUES ";

		    foreach ($content as $key => $value){

		    	$sql += "('".$_GET['modelId']."','".$key."','".$value."')";
		    }

		    $sql += ";";
		    $result = $mysql->query($sql);

		    if($result){
		    	$return["DB"] = true;
		    }
		}

	}

	echo json_encode($return);

} else {
    http_response_code(401); //Unauthorized
    die("Error: Ungültiger Nutzer.");
}

?>