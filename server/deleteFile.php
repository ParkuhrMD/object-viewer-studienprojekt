<?php

require_once "classes/util.php";

if($_SESSION["angemeldet"]){

	$return = [];

	if(isset($_GET['modelId']) && !empty($_GET['modelId'])){
        if(isset($_GET['filename']) && !empty($_GET['filename'])){

        	$sql = "SELECT * FROM `".$mysql_database."`.`Models` WHERE `modelId` = '".$_GET['modelId']."' AND `modelOwner` = '".$_SESSION["userData"]["id"]."';" ;
            $result = $mysql->query($sql);

            if ($result && $result->num_rows == 1) {

	        	$location = $modelsDir."/".$_SESSION["userData"]["id"]."/".$_GET['modelId']."/".$_GET['filename'];

	        	if(file_exists($location) && !is_dir($location)){
	        		if(unlink($location)){
	        			$return["erfolg"] = "Datai erfolgreich gelöscht";
	        		}
	        	}
	        } else {
    			$return["error"][] = "Keine Ergebnisse in der DB!";
                $return["error"][] = $sql;
    		}
        } else {
    		$return["error"][] = "Keine filename übergeben!";
    	}
    } else {
    	$return["error"][] = "Keine modelId übergeben!";
    }

    echo json_encode($return);

} else {
	http_response_code(404); //Not Found
	die("Error: File not found.");
}
?>