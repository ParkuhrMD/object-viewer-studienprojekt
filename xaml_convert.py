"""Script to erase all Geometry data from a model XAML and only leave the general structure of the objects"""

import xml.etree.ElementTree as ET

tree = ET.parse('parasolid_pumpenstand_gefaerbt_v2.xaml')

root=tree.getroot()

model_root = root[5]

ns = { '':"http://schemas.microsoft.com/winfx/2006/xaml/presentation", 'x':"http://schemas.microsoft.com/winfx/2006/xaml"}

for model in model_root.iter('{'+ns['']+'}'+'GeometryModel3D'):
    for subelem in list(model):
        model.remove(subelem)

tree.write('output.xml')