/**
 * Created by jan-niklasfreundt on 25/10/16.
 */

//TREE VIEW DELEGATE
//treeViewNumberOfElements(treeView, treeElement): Gibt Elementanzahl zurueck

TreeView = function(id, delegate, document, showsSearchBar) {
    this.id = id;
    this.document = document;
    this.delegate = delegate;
    this.treeParent = null;
    this.numberOfRootElements = 0;
    this.view = null;
    this.searchBar = null;
    _isFiltering = false;
    initView();

    this.isFiltering = function() {
        return _isFiltering;
    };

    this.showsSearchBar = showsSearchBar;

    function initView() {
        console.log("create tree view")
        this.view = document.createElement("div");

        numberOfRootElements = delegate.treeViewNumberOfElements(this,null);

        treeParent = document.getElementById(id);
        
        if(showsSearchBar) {
            this.searchBar = document.createElement("input");
            this.searchBar.type = "text";
            this.searchBar.placeholder = "Search...";
            this.searchBar.style.width = "95%";
            this.searchBar.addEventListener('input', function(sender)
            {
                console.log('input changed to: ', this.value);
                if(this.value=='') {
                    _isFiltering = false;
                } else {
                    _isFiltering = true;
                }
                console.log("isFiltering", _isFiltering);
                delegate.treeViewSearchFor(this, this.value);
            });
            treeParent.appendChild(this.searchBar);
        }
        treeParent.appendChild(view);

    }

    this.reloadData = function() {
        view.innerHTML = "";
        recursiveReload(null,0, false);
    };

    function recursiveReload(parent, layer, hidden) {
        
        var elements = new Array();


        var nextLayer = layer+1;

        var count = delegate.treeViewNumberOfElements(this,parent);
        for(var i=0; i<count; i++) {
            var child = document.createElement("div");

            var element = delegate.treeViewGetElementForIndex(this, parent, i);
            var domElement = delegate.treeViewGetDOMForElement(this,element, layer);
            domElement.representation = element;
            var hideChildren = false;
            if(hidden == false) {
                child.appendChild(domElement);
                hideChildren = element.childrenHidden;
            } else {
                hideChildren = true;
            }
            
            this.view.appendChild(child);
            child.addEventListener("click", function(sender) {
                //delegate.treeViewClickedElement()
                var domElement = sender.target;
                var data = domElement.representation;
                delegate.treeViewClickedElement(this,data);

            })
            
            
            if(hideChildren == false) {
                recursiveReload(element, nextLayer, hideChildren);
            }
        }
    }

}