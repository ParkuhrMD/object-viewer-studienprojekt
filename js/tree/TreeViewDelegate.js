/**
 * Created by jan-niklasfreundt on 25/10/16.
 */

var treeData = null;
var treeFilteredData = null;
var treeView = null;


function initView(dataOrigin) {
    this.treeData = new TreeData("3D-Model");

    buildTreeDataRecursively(dataOrigin, this.treeData);

    treeView = new TreeView("treeView", this, this.document, true);
    treeView.reloadData();
}

function buildTreeDataRecursively(dataOrigin, parent) {
    var children = dataOrigin.children;
    for(var i=0; i<children.length; i++) {
        var child = children[i];

        var title = getName(child);
        var subTreeData = new TreeData(title);
        parent.addChild(subTreeData);
        buildTreeDataRecursively(child,subTreeData);
    }
}

function getNameXML(object) {
    return object.attributes['Name'].nodeValue;
}

function getNameScene(object) {
    var title = object.name;
    if(title == ("")) { //no title? -> use type
        title = object.type; // (e.g. ambient light, group, etc.)
    }
    return title;
}

var getName = getNameXML;

function treeViewNumberOfElements(treeView, treeElement) {
    if(treeElement == null) {
        return 1;
    }

    return treeElement.getNumberOfChildren();
}

function treeViewGetElementForIndex(treeView2, parent, index) {
    if(parent == null) {
        console.log("treeview filter", this.treeView.isFiltering());

        if(this.treeView.isFiltering() == false) {
            return treeData;
        } else {
            return treeFilteredData;
        }
    }
    return parent.getChild(index);
}

function treeViewGetDOMForElement(tv, element, layer) {
    var divContainer = document.createElement("div");
    var div = document.createElement("div");

    var zoomButton = document.createElement("button");
    zoomButton.innerHTML = "Zoom";
    zoomButton.style.marginTop = "15px";
    zoomButton.representation = element;

    zoomButton.addEventListener("click", function(sender) {

        var element = this.representation;
        if(element != null) {

            var name = element.getName();
            if(name != null && name != undefined) {
                console.log("focus element: "+element.getName());
                var object = scene.getObjectByName(name);
                focusObject(object);
            }

       //     treeView.reloadData();
            resizeContent();
        }
    });

    var selectButton = document.createElement("button");
    selectButton.innerHTML = "Select";

    var currentSelection = currentSelectedObject;
    if(currentSelectedObject != null) {
        var selectedName = currentSelectedObject.name;
        var name = element.getName();
        if(name == selectedName) {
            selectButton.innerHTML = "Unselect";
            divContainer.style.backgroundColor = '#3891DC';
        }
    }

    selectButton.style.marginTop = "15px";
    selectButton.representation = element;
    selectButton.addEventListener("click", function(sender) {
        var element = this.representation;

        if(element != null) {

            var name = element.getName();
            if(name != null && name != undefined) {
                var selected = false;

                if(currentSelection != null) {

                    var selectedName = currentSelection.name;
                    if(name == selectedName) {
                        selected = true;
                    }
                }

                if(selected == false) {
                    selectObject(scene.getObjectByName(name));
                } else {
                    deselectAllObjects();
                }

                 //this.treeView.reloadData();
            }
        }
    });

    //Expand/shrink button
    var expandButton = document.createElement("button");

    if(element.childrenHidden) {
        expandButton.innerHTML = " + ";

    } else {
        expandButton.innerHTML = " - ";

    }
    expandButton.style.marginTop = "15px";
    expandButton.style.float = 'right';
    expandButton.representation = element;
    expandButton.addEventListener("click", function(sender) {
         if(element.childrenHidden) {
            element.childrenHidden = false;
        } else {
            element.childrenHidden = true;
        }
    })

    if(element.hasChildren()) {
        div.appendChild(expandButton);
    }


    //Calculate margin
    div.style.marginLeft = (layer*20)+"px";

    var span = document.createElement("span");
    span.addEventListener("click", function(sender) {
        //delegate.treeViewClickedElement()
        var domElement = sender.target;
        var data = domElement.parentNode.representation;
        console.log("clicked span", data);
        treeViewClickedElement(this,data);

    })
    span.classList.add("noselect");
    span.innerHTML = element.getName();
    span.style.marginTop = '15px';
    span.style.width = '100px';
    div.appendChild(span);

    div.classList.add("treeCell");

    if(element.hasChildren() == false) {
        div.appendChild(zoomButton);
        div.appendChild(selectButton);
    }
    divContainer.appendChild(div);
    return divContainer
}

function treeViewClickedElement(treeView2, element) {
    console.log("this.treeView:", this.treeView);
    treeView.reloadData();

    if(element != null) {

        var name = element.getName();
        if(name != null && name != undefined) {
            console.log("clicked element: "+element.getName());
            var object = scene.getObjectByName(name);

            requestTextBox(object);
        }
    }
}


function treeViewSearchFor(treeView2,searchString) {
    this.treeFilteredData = getFilteredDataFor(this.treeData,searchString)[0];
    treeView.reloadData();
}

function getFilteredDataFor(treeElement, filter, f) {
    var count = treeElement.getNumberOfChildren();
    var newElement = new TreeData(treeElement.getName());

    var found = f

    if(treeElement.hasChildren() == false) {
        if (newElement.getName().toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
            console.log(newElement.getName(), 'includes', filter);
            found = true;
        } else {
            return [null, false];
        }
    } else {
        for (var i = 0; i < count; i++) {
            var subelement = treeElement.getChild(i);

            if (subelement.getName().toLocaleLowerCase().indexOf(filter.toLowerCase()) !== -1) {
                found = true;
            }

            var returnValue = getFilteredDataFor(subelement,filter, found);
            var ele = returnValue[0];

            if(returnValue[1] == true) {
                found = true;
            }

            if(found && ele != null) {
                newElement.addChild(ele);
            }
        }
    }

    return [newElement,found];
}
